const tabs = document.querySelectorAll('.tabs li');

for (let i = 0; i < tabs.length; i++) {

    tabs[i].addEventListener('click', function () {
        this.classList.add('active');
        let sibling = this.previousElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.previousElementSibling;
        }
        sibling = this.nextElementSibling;
        while (sibling) {
            sibling.classList.remove('active');
            sibling = sibling.nextElementSibling;
        }

        const tabContainer = document.querySelector('.tabs-content').children;

        const tabContent = tabContainer[i];

        for (let i = 0; i < tabContainer.length; i++) {
            tabContainer[i].classList.remove('active');
        }
        tabContent.classList.add('active');
    })
}
